package com.ahmed.annexe15;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance; // unique instance of the Singleton
    private SQLiteDatabase database;

    private Context context;

    public static DatabaseHelper getInstance(Context context){
        if(instance == null)
            instance = new DatabaseHelper(context.getApplicationContext());
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, "quebec", null, 1);  // context, name, factory, version
        this.context = context;
    }


    public int executerCommandesFichier( Context context, int resourceId, SQLiteDatabase db) throws IOException {

        int compte = 0;

        InputStream is = context.getResources().openRawResource(resourceId); // flux de communication de donnees brutes
        InputStreamReader isr = new InputStreamReader(is); // flux de traduction : donnees brutes a des donnees caracteres

        // buffer / tampon
        BufferedReader br = new BufferedReader(isr); // buffer : permet la lecture de ligne plutot qu'un char a la fois

        while(br.ready()){ // while ( br.readLine() != null ) --> Do not use this

            String enonce = br.readLine();
            db.execSQL(enonce);
            compte++;

        }

        br.close(); // fermer le flux de donnees

        return compte;
    }


    // called automatically when the app is installed ( 1 time )
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE inventeur (_id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, origine TEXT, invention TEXT, annee INTEGER);"); // create the table
    }


    // called automatically when the phone detects that 1 database with the same name already exists on the phone
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int ancVersion, int nlleVersion) {

        onCreate(sqLiteDatabase);
    }

    public void ouvrirBD(){
        database = this.getWritableDatabase();
    }

    public void fermerBD(){
        database.close(); // fermer la connexion
    }

    public Vector<String> retournerInventions (){

        Vector<String> v = new Vector<>();
        Cursor c = database.rawQuery("SELECT invention FROM inventeur", null);

        while(c.moveToNext()){ // tant qu'il y a des resultat encore
            v.add(c.getString(0)); // j'ajoute l'invention dans le vecteur
        }
        return v;
    }

    public Boolean vraiInventeur(String inventeur, String invention){

        String[] tab = { invention, inventeur };
        Cursor c = database.rawQuery("SELECT invention FROM inventeur WHERE invention = ? AND nom = ?", tab);
        return c.moveToNext(); // vrai ou faux
    }
}
