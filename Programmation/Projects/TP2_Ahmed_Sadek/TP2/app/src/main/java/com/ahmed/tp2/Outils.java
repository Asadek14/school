package com.ahmed.tp2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.widget.LinearLayout;

public class Outils{

    private Ligne ligne;
    private Rectangle rectangle;
    private Cercle cercle;
    private Triangle triangle;
    private String action;
    private String formeCourrant;
    private int couleurCourrante;
    private int couleurDuFondCourrante;
    private int largeurCourrante;

    public Outils() {

        ligne = new Ligne("ligne", new Path(), 0, 0);
        rectangle = new Rectangle("rectangle", new Point(), new Point(), 0 ,0);
        cercle = new Cercle("cercle", new Point(), new Point(), 0 ,0);
        triangle = new Triangle("triangle", new Point(), new Point(), new Point(), 0 ,0);
        action = "dessiner";
        formeCourrant = "ligne";
        couleurCourrante = Color.BLACK;
        couleurDuFondCourrante = Color.WHITE;
        largeurCourrante = 10;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }



    public String getFormeCourrant() {
        return formeCourrant;
    }

    public void setFormeCourrant(String formeCourrant) {
        this.formeCourrant = formeCourrant;
    }



    public int getCouleurCourrante() {
        return couleurCourrante;
    }

    public void setCouleurCourrante(int couleurCourrante) {
        this.couleurCourrante = couleurCourrante;
    }



    public int getCouleurDuFondCourrante() {
        return couleurDuFondCourrante;
    }

    public void setCouleurDuFondCourrante(int couleurDuFondCourrante) {
        this.couleurDuFondCourrante = couleurDuFondCourrante;
    }



    public int getLargeurCourrante() {
        return largeurCourrante;
    }

    public void setLargeurCourrante(int largeurCourrante) {
        this.largeurCourrante = largeurCourrante;
    }



    public Ligne getLigne() {
        return ligne;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public Cercle getCercle() {
        return cercle;
    }

    public Triangle getTriangle() {
        return triangle;
    }

    // Fonction pour dessiner un forme dans la liste des formes
    public void canvasDrawDessin(Canvas canvas, Paint paint, Forme dessin){
        paint.setColor(dessin.getCouleur());
        paint.setStrokeWidth(dessin.getLargeur());

        Point depart;
        Point arrivee;

        switch(dessin.getNom()){

            case "ligne":
                if(((Ligne) dessin).isEfface())
                    paint.setColor(getCouleurDuFondCourrante());
                canvas.drawPath(((Ligne) dessin).getPath(), paint);
                break;

            case "rectangle":
                depart = ((Rectangle) dessin).getDepart();
                arrivee = ((Rectangle) dessin).getArrivee();
                canvas.drawRect(depart.x, depart.y, arrivee.x, arrivee.y, paint);
                break;

            case "cercle":
                depart = ((Cercle) dessin).getDepart();
                arrivee = ((Cercle) dessin).getArrivee();
                canvas.drawOval(depart.x, depart.y, arrivee.x, arrivee.y, paint);
                break;

            case "triangle":
                Point sommet1 = ((Triangle) dessin).getSommet1();
                Point sommet2 = ((Triangle) dessin).getSommet2();
                Point sommet3 = ((Triangle) dessin).getSommet3();

                canvas.drawLine(sommet1.x, sommet1.y, sommet2.x, sommet2.y, paint);
                canvas.drawLine(sommet1.x, sommet1.y, sommet3.x, sommet3.y, paint);
                canvas.drawLine(sommet3.x, sommet3.y, sommet2.x, sommet2.y, paint);
                break;
        }
    }

    // Fonction pour dessiner le forme qui est en train d'etre dessiner
    public void canvasDrawDessinCourrant(Canvas canvas, Paint paint){
        paint.setColor(getCouleurCourrante());
        paint.setStrokeWidth(getLargeurCourrante());

        Point depart;
        Point arrivee;

        switch(getFormeCourrant()){

            case "ligne":
                if(getAction().equals("effacer"))
                    paint.setColor(getCouleurDuFondCourrante());
                canvas.drawPath(getLigne().getPath(), paint);
                break;

            case "rectangle":
                depart = getRectangle().getDepart();
                arrivee = getRectangle().getArrivee();
                canvas.drawRect(depart.x, depart.y, arrivee.x, arrivee.y, paint);
                break;

            case "cercle":
                depart = getCercle().getDepart();
                arrivee = getCercle().getArrivee();
                canvas.drawOval(depart.x, depart.y, arrivee.x, arrivee.y, paint);
                break;

            case "triangle":
                Point sommet1 = getTriangle().getSommet1();
                Point sommet2 = getTriangle().getSommet2();
                Point sommet3 = getTriangle().getSommet3();

                if(getTriangle().isComplete()) {
                    canvas.drawLine(sommet1.x, sommet1.y, sommet2.x, sommet2.y, paint);
                    canvas.drawLine(sommet1.x, sommet1.y, sommet3.x, sommet3.y, paint);
                    canvas.drawLine(sommet3.x, sommet3.y, sommet2.x, sommet2.y, paint);
                }
                else
                    canvas.drawLine(sommet1.x, sommet1.y, sommet2.x, sommet2.y, paint);
                break;
        }

    }

}
