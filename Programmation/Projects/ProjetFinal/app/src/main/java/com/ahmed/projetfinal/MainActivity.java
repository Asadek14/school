package com.ahmed.projetfinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Build;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Space;

public class MainActivity extends AppCompatActivity {

    LinearLayout parentJeu;
    LinearLayout parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = findViewById(R.id.parent);
        parentJeu = findViewById(R.id.parent_jeu);

        EcouteurDragDefaut ecDD = new EcouteurDragDefaut();
        EcouteurDragCartes ecDC = new EcouteurDragCartes();
        EcouteurDragCollecte ecDCO =  new EcouteurDragCollecte();

        parent.setOnDragListener(ecDD);

        for(int i = 0; i < parentJeu.getChildCount(); i++){

            if(!(parentJeu.getChildAt(i) instanceof ImageButton)) {
                LinearLayout conteneur = (LinearLayout) parentJeu.getChildAt(i);
                for (int j = 0; j < conteneur.getChildCount(); j++) {



                    if (i == 0 || i == 2) { // les 2 conteneurs de collectes

                        LinearLayout sousConteneur = (LinearLayout) conteneur.getChildAt(j);
                        ConstraintLayout conteneurCollecte = (ConstraintLayout) sousConteneur.getChildAt(1);
                        conteneurCollecte.setOnDragListener(ecDCO);


                    } else { // les 2 conteneurs de cartes

                        if (!(conteneur.getChildAt(j) instanceof Space)) {
                            ConstraintLayout conteneurCarte = (ConstraintLayout) conteneur.getChildAt(j);
                            conteneurCarte.setOnDragListener(ecDC);
                            conteneurCarte.getChildAt(0).setOnTouchListener(ecDC);
                        }

                    }
                }
            }
        }
    }

    private class EcouteurDragCartes implements View.OnDragListener, View.OnTouchListener{


        View carte = null;

        @Override
        public boolean onDrag(View source, DragEvent dragEvent) {

            switch (dragEvent.getAction()){

                case DragEvent.ACTION_DROP:


                    // Chercher la carte en question
                    carte = (View) dragEvent.getLocalState();

                    // Chercher le conteneur en question
                    ConstraintLayout conteneur = (ConstraintLayout) source;
                    if(conteneur.getChildCount() == 0){

                        // Chercher l'ancien conteneur de cette carte
                        ConstraintLayout parent = (ConstraintLayout)carte.getParent();
                        parent.removeView(carte);
                        conteneur.addView(carte);
                    }

                    carte.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }

            return true;
        }

        @Override
        public boolean onTouch(View source, MotionEvent motionEvent) {

            // c'est l'ombre qu'on va transporter d'une colonne a l'autre
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(source);

            source.startDragAndDrop(null, shadowBuilder, source, 0);

            source.setVisibility(View.INVISIBLE); // pour cache le jeton tant qu'il n'est pas deplace
            return true;
        }

    }

    private class EcouteurDragDefaut implements View.OnDragListener{


        View carte = null;

        @Override
        public boolean onDrag(View source, DragEvent dragEvent) {

            switch (dragEvent.getAction()){

                case DragEvent.ACTION_DROP:
                    carte = (View) dragEvent.getLocalState();

                    // remet le jeton visible
                    carte.setVisibility(View.VISIBLE);

                    break;

                default:
                    break;
            }

            return true;
        }

    }

    private class EcouteurDragCollecte implements View.OnDragListener {


        View carte = null;

        @Override
        public boolean onDrag(View source, DragEvent dragEvent) {

            switch (dragEvent.getAction()) {

                case DragEvent.ACTION_DROP:


                    // Chercher la carte en question
                    carte = (View) dragEvent.getLocalState();

                    // Chercher le conteneur en question
                    ConstraintLayout conteneur = (ConstraintLayout) source;

                    // Chercher l'ancien conteneur de cette carte
                    ConstraintLayout parent = (ConstraintLayout) carte.getParent();
                    parent.removeView(carte);
                    conteneur.addView(carte);

                    carte.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }

            return true;
        }
    }
}