package com.ahmed.annexe7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    ConstraintLayout parent;
    Surface surf;
    Ecouteur ec;
    Point depart, arrive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = findViewById(R.id.PARENT);

        surf = new Surface(this);
        surf.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        surf.setBackgroundResource(R.drawable.carte);
        parent.addView(surf);

        ec = new Ecouteur();
        surf.setOnTouchListener(ec);
    }
    private class Ecouteur implements View.OnTouchListener{

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int action = event.getAction();

            if(action == event.ACTION_DOWN){
                depart = new Point((int) event.getX(), (int) event.getY());
                surf.invalidate();
            }else if(action == event.ACTION_MOVE){
                arrive = new Point((int) event.getX(), (int) event.getY());
                surf.invalidate();
            }else if(action == event.ACTION_UP){
                depart = null;
                arrive = null;
            }else{
                System.out.println("error");
            }
            return true;
        }
    }
    private class Surface extends View{

        Paint crayon;
        Rect rectangle;

        public Surface(Context context) {
            super(context);

            crayon = new Paint(Paint.ANTI_ALIAS_FLAG);
            crayon.setColor(Color.RED);
            crayon.setStrokeWidth(15);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if(depart != null){
                canvas.drawRect(depart.x - 30, depart.y - 30, depart.x + 30, depart.y + 30, crayon);
            }
            if(arrive != null){
                canvas.drawRect(arrive.x - 30, arrive.y - 30, arrive.x + 30, arrive.y + 30, crayon);
                canvas.drawLine(depart.x, depart.y, arrive.x, arrive.y, crayon);
            }
        }
    }
}