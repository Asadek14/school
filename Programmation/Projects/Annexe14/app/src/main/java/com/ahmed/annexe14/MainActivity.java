package com.ahmed.annexe14;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    LinearLayout conteneur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Ecouteur ec =  new Ecouteur();
        conteneur = findViewById(R.id.conteneur);

        for(int i = 0; i < conteneur.getChildCount(); i++){

            LinearLayout colonne = (LinearLayout) conteneur.getChildAt(i);
            colonne.setOnDragListener(ec);
            colonne.getChildAt(0).setOnTouchListener(ec);
        }


    }


    private class Ecouteur implements View.OnDragListener, View.OnTouchListener{


        Drawable normal = getResources().getDrawable(R.drawable.triangle_background);
        Drawable select = getResources().getDrawable(R.drawable.triangle_background_selectionne);
        View jeton = null;


	// Drawable normal = getResources().getDrawable(R.drawable.triangle_background, null);
        // Drawable select = getResources().getDrawable(R.drawable.triangle_background_selectionne, null);


	// Drawable normal = ContextCompat().getDrawable(MainActivity.this, R.drawable.triangle_background);
        // Drawable select = ContextCompat().getDrawable(MainActivity.this, R.drawable.triangle_background_selectionne);


        @Override
        public boolean onDrag(View source, DragEvent dragEvent) {


            switch (dragEvent.getAction()){
                case DragEvent.ACTION_DRAG_ENTERED:
                    source.setBackground(select);
                    break;

                case DragEvent.ACTION_DROP:

                    // le jeton reste sur la colonne de depart ( parametre source de startDragAndDrop)

                    // ( this step is related to  source.startDragAndDrop(null, shadowBuilder, source, 0); on the onTouch event
                    // set for each one of the buttons )
                    jeton = (View) dragEvent.getLocalState();

                    // aller chercher le contenteur d'origine
                    LinearLayout parent = (LinearLayout)jeton.getParent();

                    // enlever le jeton du conteneur d'origine ( car on l'avait seulement rendu invisible )
                    parent.removeView(jeton);

                    // colonne ou je suis rendu
                    LinearLayout conteneur = (LinearLayout)source;

                    // ajoute le jeton
                    conteneur.addView(jeton);

                    // remet le jeton visible
                    jeton.setVisibility(View.VISIBLE);

                    break;


                case DragEvent.ACTION_DRAG_EXITED:
                    source.setBackground(normal);
                    break;

                case DragEvent.ACTION_DRAG_ENDED:
                    source.setBackground(normal);
                    break;

                default:
                    break;
            }






            return true;
        }

        @Override
        public boolean onTouch(View source, MotionEvent motionEvent) {

            // c'est l'ombre qu'on va transporter d'une colonne a l'autre
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(source);


            // partie optionnelle
            int sdkVersion = Build.VERSION.SDK_INT;
            if(sdkVersion <= 24)
                source.startDrag(null, shadowBuilder, source, 0);
            else
                source.startDragAndDrop(null, shadowBuilder, source, 0);
            // partie optionnelle

            source.setVisibility(View.INVISIBLE); // pour cache le jeton tant qu'il n'est pas deplace

            return true;
        }
    }
}