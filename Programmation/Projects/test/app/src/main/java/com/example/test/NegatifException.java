package com.example.test;

public class NegatifException extends Exception {

    // classe exception : String message, getMessage, contructeur ( message )

    private double montantErrone;

    public NegatifException(double montantErrone){

        super("Gaston, entrez un montant > 0 svp :)");
        this.montantErrone = montantErrone;
    }

    public double getMontantErrone(){
        return montantErrone;
    }
}
