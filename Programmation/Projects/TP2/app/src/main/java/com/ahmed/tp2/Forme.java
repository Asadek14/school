package com.ahmed.tp2;

public class Forme {

    private String nom;
    private int couleur;
    private int largeur;

    public Forme(String nom, int couleur, int largeur) {
        this.nom = nom;
        this.couleur = couleur;
        this.largeur = largeur;
    }

    public String getNom() {
        return nom;
    }

    public int getCouleur() {
        return couleur;
    }

    public int getLargeur() {
        return largeur;
    }
}
