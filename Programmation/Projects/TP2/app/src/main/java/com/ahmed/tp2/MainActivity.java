package com.ahmed.tp2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    EcouteurCouleur ecCouleur;
    EcouteurSurface ecSurface;
    EcouteurOutil ecOutil;

    ConstraintLayout conteneurSurface;
    Surface surface;
    Paint paint;

    LinearLayout conteneurCouleurs;
    TextView champsCouleurCourrante;
    TextView champsOuitlCourrant;

    LinearLayout conteneurOutils;
    Outils outils; // Objet qui sert a contenir tous les formes et les actions courrants

    Vector<Forme> listeDessins;

    LargeurTrait largeurTrait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialisation des Ecouteurs
        ecCouleur = new EcouteurCouleur();
        ecSurface = new EcouteurSurface();
        ecOutil = new EcouteurOutil();

        // Initialisation de la surface du dessin
        conteneurSurface = findViewById(R.id.DRAWING_SURFACE);
        surface = new Surface(this);
        surface.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        conteneurSurface.addView(surface);
        surface.setOnTouchListener(ecSurface);

        // Initialisation des couleurs
        conteneurCouleurs = findViewById(R.id.COLORS_CONTAINER);
        champsCouleurCourrante = findViewById(R.id.CURRENT_COLOR);

        // for the colors listener
        for(int b = 0; b < conteneurCouleurs.getChildCount(); b++){
            if(conteneurCouleurs.getChildAt(b) instanceof Button){
                conteneurCouleurs.getChildAt(b).setOnClickListener(ecCouleur);
            }
        }

        // Initialisation des outils de dessin

        conteneurOutils = findViewById(R.id.UTILITIES_CONTAINER);
        champsOuitlCourrant = findViewById(R.id.TOOL);
        for(int b = 0; b < conteneurOutils.getChildCount(); b++){
            if(conteneurOutils.getChildAt(b) instanceof ImageButton){
                System.out.println("hello");
                conteneurOutils.getChildAt(b).setOnClickListener(ecOutil);
            }
        }

        // Initailisation du vecteur de dessins
        listeDessins = new Vector<Forme>();


        // Initialisation des outils de dessin
        outils = new Outils();

    }

    // Ecouteur pour les buttons de couleur
    private class EcouteurCouleur implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            // Puisque les button ont des backgrounds drawable et non pas seulement une couleur,
            // il fallait utilisè ce code pour recuperer leur couleur

            Button button = (Button)view;
            StateListDrawable drawable = (StateListDrawable) button.getBackground();
            DrawableContainer.DrawableContainerState state = (DrawableContainer.DrawableContainerState) drawable.getConstantState();
            Drawable[] children = state.getChildren();
            GradientDrawable background = (GradientDrawable) children[0];

            outils.setCouleurCourrante(background.getColor().getDefaultColor());
            champsCouleurCourrante.setBackgroundColor(outils.getCouleurCourrante());
        }
    }

    // Ecouteur pouer les buttons des outils en bas
    private class EcouteurOutil implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            // Cette partie sert a modifier les actions et les formes courrant au moment d'un clique
            String forme = view.getTag().toString();
            switch(forme) {
                case "ligne":
                case "rectangle":
                case "cercle":
                case "triangle":
                    outils.setAction("dessiner");
                    outils.setFormeCourrant(forme);
                    champsOuitlCourrant.setText(forme);
                    break;
                case "efface":
                    outils.setAction("effacer");
                    outils.setFormeCourrant("ligne");
                    champsOuitlCourrant.setText("efface");
                    break;
                case "seau":
                    outils.setCouleurDuFondCourrante(outils.getCouleurCourrante());
                    surface.invalidate();
                    outils.setAction("dessiner");
                    outils.setFormeCourrant("ligne");
                    champsOuitlCourrant.setText("ligne");
                    break;
                case "pipette":
                    outils.setAction("pipette");
                    outils.setFormeCourrant("none");
                    champsOuitlCourrant.setText("pipette");
                    break;
                case "trait":
                    largeurTrait = new LargeurTrait(MainActivity.this, outils);
                    largeurTrait.show();
                    break;
            }
        }
    }


    // Ecouteur pour les touches sur la surface
    private class EcouteurSurface implements View.OnTouchListener{

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if(!outils.getFormeCourrant().equals("none")){

                // Initialsization ou modifications des coordonnees x,y pour chaque sommet d'une forme
                switch(outils.getFormeCourrant()){
                    case "ligne":     outils.getLigne().onTouch(event, listeDessins, outils); break;
                    case "rectangle": outils.getRectangle().onTouch(event, listeDessins, outils); break;
                    case "cercle":    outils.getCercle().onTouch(event, listeDessins, outils); break;
                    case "triangle":  outils.getTriangle().onTouch(event, listeDessins, outils); break;
                }
            }else if(outils.getAction().equals("pipette")){

                int couleurPixel = surface.getBitmapImage().getPixel((int)event.getX(), (int)event.getY());
                outils.setCouleurCourrante(couleurPixel);
                champsCouleurCourrante.setBackgroundColor(outils.getCouleurCourrante());
            }

            surface.invalidate();
            return true;
        }
    }


    private class Surface extends View {

        public Surface(Context context) {
            super(context);

            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawColor(outils.getCouleurDuFondCourrante());

            for(Forme dessin: listeDessins)
                // Fonction pour dessiner un forme dans la liste des formes
                outils.canvasDrawDessin(canvas, paint, dessin);

            // Fonction pour dessiner le forme qui est en train d'etre dessiner
            outils.canvasDrawDessinCourrant(canvas, paint);

        }

        public Bitmap getBitmapImage() {

            this.buildDrawingCache();
            Bitmap bitmapImage = Bitmap.createBitmap(this.getDrawingCache());
            this.destroyDrawingCache();

            return bitmapImage;
        }

    }
}