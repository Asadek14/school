package com.ahmed.tp2;

import android.graphics.Path;
import android.graphics.Point;
import android.view.MotionEvent;

import java.util.Vector;

public class Rectangle extends Forme {

    Point depart, arrivee;

    public Rectangle(String nom, Point depart, Point arrivee, int couleur, int largeur) {
        super(nom, couleur, largeur);

        this.depart = depart;
        this.arrivee = arrivee;
    }

    public void setDepart(int x, int y) {

        depart.x = x;
        depart.y = y;
        arrivee.x = x;
        arrivee.y = y;
    }

    public void setArrivee(int x, int y) {
        arrivee.x = x;
        arrivee.y = y;
    }

    public Point getDepart() {
        return depart;
    }

    public Point getArrivee() {
        return arrivee;
    }


    // Fonction qui sert a initializer ou modifier les coordonnees x,y pour le rectangle
    public void onTouch(MotionEvent event, Vector<Forme> listeDessins, Outils outils) {

        int action = event.getAction();
        int couleur = outils.getCouleurCourrante();
        int largeur = outils.getLargeurCourrante();

        if (action == event.ACTION_DOWN) {
            setDepart((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_MOVE) {
            setArrivee((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_UP) {
            setArrivee((int) event.getX(), (int) event.getY());
            listeDessins.add(new Rectangle("rectangle", new Point(getDepart().x, getDepart().y), new Point(getArrivee().x, getArrivee().y), couleur, largeur));
            reset();
        }
    }

    public void reset(){
        depart = new Point();
        arrivee = new Point();
    }

}
