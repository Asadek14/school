package com.ahmed.tp2;

import android.graphics.Path;
import android.graphics.Point;
import android.view.MotionEvent;

import java.util.Vector;

public class Triangle extends Forme {

    private Point sommet1, sommet2, sommet3;
    private boolean firstTouch = true;

    public Triangle(String nom, Point sommet1, Point sommet2, Point sommet3, int couleur, int largeur) {
        super(nom, couleur, largeur);

        this.sommet1 = sommet1;
        this.sommet2 = sommet2;
        this.sommet3 = sommet3;
    }

    public void setSommet1(int x, int y) {
        sommet1.x = x;
        sommet1.y = y;
        sommet2.x = x;
        sommet2.y = y;
    }

    public void setSommet2(int x, int y) {
        sommet2.x = x;
        sommet2.y = y;
        sommet3.x = x;
        sommet3.y = y;
    }

    public void setSommet3(int x, int y) {
        sommet3.x = x;
        sommet3.y = y;
    }

    // Fonction qui sert a initializer ou modifier les coordonnees x,y pour le triangle
    public void onTouch(MotionEvent event, Vector<Forme> listeDessins, Outils outils) {

        int action = event.getAction();
        int couleur = outils.getCouleurCourrante();
        int largeur = outils.getLargeurCourrante();


        if (action == event.ACTION_DOWN) {
            if(firstTouch)
                setSommet1((int) event.getX(), (int) event.getY());
            else
                setSommet3((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_MOVE) {
            if(firstTouch)
                setSommet2((int) event.getX(), (int) event.getY());
            else
                setSommet3((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_UP) {
            if(firstTouch){
                firstTouch = false;
            }
            else{
                listeDessins.add(new Triangle("triangle", new Point(getSommet1().x, getSommet1().y), new Point(getSommet2().x, getSommet2().y), new Point(getSommet3().x, getSommet3().y), couleur, largeur));
                reset();
            }
        }
    }

    public Point getSommet1() {
        return sommet1;
    }

    public Point getSommet2() {
        return sommet2;
    }

    public Point getSommet3() {
        return sommet3;
    }

    public boolean isComplete() {
        return !firstTouch;
    }

    public void reset(){
        sommet1 = new Point();
        sommet2 = new Point();
        sommet3 = new Point();
        firstTouch = true;
    }
}
