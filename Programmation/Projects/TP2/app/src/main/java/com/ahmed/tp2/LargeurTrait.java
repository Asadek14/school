package com.ahmed.tp2;

import android.app.Dialog;
import android.content.Context;
import android.media.tv.TvView;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LargeurTrait extends Dialog {

    Ecouteur ec;
    Button button;
    SeekBar seekBar;
    MainActivity parent;
    Outils outils;

    public LargeurTrait(@NonNull Context context, Outils outils){
        super(context);
        parent = (MainActivity) context;
        this.outils = outils;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.largeur_trait);

        button = findViewById(R.id.buttonValidation);
        seekBar = findViewById(R.id.largeurTrait);

        ec = new Ecouteur();

        button.setOnClickListener(ec);

    }

    private class Ecouteur implements View.OnClickListener {


        @Override
        public void onClick(View view) {

//            System.out.println(seekBar.getProgress());
            outils.setLargeurCourrante(seekBar.getProgress());
            dismiss();
        }
    }
}
