package com.ahmed.tp2;

import android.graphics.Path;
import android.graphics.Point;
import android.view.MotionEvent;

import java.util.Vector;

public class Ligne extends Forme{

    private Path path;
    private boolean efface;

    public Ligne(String nom, Path path, int couleur, int largeur) {
        super(nom, couleur, largeur);
        this.path = path;
        this.efface = false;
    }

    public Ligne(String nom, Path path, int couleur, int largeur, boolean efface) {
        super(nom, couleur, largeur);
        this.path = path;
        this.efface = efface;
    }

    public void setDepart(int x, int y) {

        path.moveTo(x ,y);
    }

    // Fonction qui sert a initializer ou modifier l'objet path de la ligne
    public void onTouch(MotionEvent event, Vector<Forme> listeDessins, Outils outils) {

        int action = event.getAction();
        int couleur = outils.getCouleurCourrante();
        int largeur = outils.getLargeurCourrante();


        if (action == event.ACTION_DOWN) {
            setDepart((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_MOVE) {
            setArrivee((int) event.getX(), (int) event.getY());

        } else if (action == event.ACTION_UP) {
            setArrivee((int) event.getX(), (int) event.getY());

            if(!outils.getAction().equals("effacer"))
                listeDessins.add(new Ligne("ligne", new Path(getPath()), couleur, largeur));
            else
                listeDessins.add(new Ligne("ligne", new Path(getPath()), couleur, largeur, true));
            reset();
        }
    }

    public void setArrivee(int x, int y) {
        path.lineTo(x, y);
    }

    public Path getPath() {
        return path;
    }

    public void reset(){
        path = new Path();
        efface = false;
    }

    public boolean pathIsNull(){
        return path == null;
    }

    public boolean isEfface() {
        return efface;
    }
}
