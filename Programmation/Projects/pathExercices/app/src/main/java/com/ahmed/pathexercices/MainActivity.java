package com.ahmed.pathexercices;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    ConstraintLayout parent;
    Surface surf;
    TextView champX, champY;
    Button entree;
    Path p;
    Ecouteur ec;
    Point coordonnne;
    Boolean premierFois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = findViewById(R.id.parent);

        surf = new Surface(this);
        surf.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        parent.addView(surf);

        coordonnne = new Point(0,0);
        premierFois = true;

        champX = findViewById(R.id.ValeurX);
        champY = findViewById(R.id.ValeurY);
        entree = findViewById(R.id.Entree);

        p = new Path();
        ec = new Ecouteur();

        entree.setOnClickListener(ec);
    }
    private class Ecouteur implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String regex = "[0-9]+";
            String valeurX = champX.getText().toString();
            String valeurY = champY.getText().toString();

            if(valeurX.matches(regex) && valeurY.matches(regex)){
                Integer x = Integer.parseInt(valeurX);
                Integer y = Integer.parseInt(valeurY);

                coordonnne.x = x;
                coordonnne.y = y;

                if(premierFois){
                    p.moveTo(coordonnne.x ,coordonnne.y);
                    premierFois = false;
                }else {
                    p.lineTo(coordonnne.x, coordonnne.y);
                }

                surf.invalidate();
            }
        }
    }

    private class Surface extends View {

        Paint crayon;

        public Surface(Context context) {
            super(context);

            crayon = new Paint(Paint.ANTI_ALIAS_FLAG);
            crayon.setColor(Color.BLUE);
            crayon.setStyle(Paint.Style.STROKE);
            crayon.setStrokeWidth(10);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawPath(p, crayon);
        }
    }
}