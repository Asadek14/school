package com.ahmed.annexe5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity {

    ConstraintLayout principal;
    Surface surf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        principal = findViewById(R.id.principal);

        // 1ere etape : creer l'objet
        surf = new Surface( this);

        // 2eme etape
        surf.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        // Donner une couleur de fond a notre surf
//        surf.setBackgroundColor(ContextCompat.getColor(this, R.color.purple_200));
//        surf.setBackgroundColor(Color.parseColor("#FF45a323"));
        surf.setBackgroundColor(Color.CYAN);

        //3eme etape
        principal.addView(surf);
    }
    public int convertirDpEnPixel (int dp ){
        return Math.round(dp * this.getResources().getDisplayMetrics().density);
    }
    private class Surface extends View {

        Paint crayon;
        public Surface(Context context) {
            super(context);
            crayon = new Paint(Paint.ANTI_ALIAS_FLAG); // arrondir les contours
            crayon.setColor(Color.GRAY);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawCircle(50, 50, 50, crayon);
        }
    }
}