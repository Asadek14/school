package com.ahmed.annexe13c;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Toast;

public class enregistrerActivity extends AppCompatActivity {

    DatabaseHelper db;
    Button buttonEnregistrer;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enregistrer);

        buttonEnregistrer = findViewById(R.id.buttonEnregistrer);
        ratingBar = findViewById(R.id.ratingBar);

        db = new DatabaseHelper(this);
        db.ouvrivBD();

        Evaluation molson = new Evaluation("Molson", "Molson", 2);
        Evaluation ale = new Evaluation("Ale", "Ale", 3);
        Evaluation budLight = new Evaluation("BudLight", "BudLight", 2.5);
        Evaluation belge = new Evaluation("Belge", "Belge", 4);

        db.ajouterEvaluation(molson);
        db.ajouterEvaluation(ale);
        db.ajouterEvaluation(budLight);
        db.ajouterEvaluation(belge);

        Ecouteur ec = new Ecouteur();
        buttonEnregistrer.setOnClickListener(ec);
        ratingBar.setOnRatingBarChangeListener(ec);
    }

    private class Ecouteur implements View.OnClickListener, RatingBar.onRatingBarChangeListener {

        @Override
        public void onClick(View v) {
            if (v.getId() == buttonEnregistrer.getId()) {
                try{
                    db.meilleuresBieres();
                    System.out.println("meilleures bieres try done");
                } catch (Exception e){
                    e.printStackTrace();
                    Toast toast = Toast.makeText(enregistrerActivity.this, "il y a moins de 3 enregistrements", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:

                    return true;
                case MotionEvent.ACTION_MOVE:

                    return true;
                case MotionEvent.ACTION_UP:

                    return true;
            }
            return true;
        }
    }
}