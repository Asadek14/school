package com.ahmed.annexe13c;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.Vector;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance;
    private SQLiteDatabase database;


    private static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DatabaseHelper(Context context) {
        super(context, "bieres", null, 1);
    }

    public void ajouterEvaluation( Evaluation e) {
        ContentValues cv = new ContentValues();
        cv.put("nom", e.getNom());
        cv.put("microbrasserie", e.getMicrobrasserie());
        cv.put("evaluation", e.getEvaluation());
        database.insert("evaluation", null, cv); // whats nullColumnHack?
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table evaluation (_id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, microbrasserie TEXT, evaluation REAL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists evaluation");
        onCreate(db);
    }

    public void meilleuresBieres() throws Exception{
        Cursor c;
        int i = 0;
        Vector<String> v = new Vector();
        c = database.rawQuery("SELECT nom FROM evaluation ORDER BY evaluation DESC LIMIT 3", null);

        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        if (v.size() < 3) {
            throw new Exception();
        }
    }

    public void ouvrivBD() {
        database = this.getWritableDatabase();
    }

    public void fermerBD() {database.close();}
}

//note Pour onclick, utiliser des Intent et changer d'activite selon le bouton clique