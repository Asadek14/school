package com.ahmed.sadek.annexe3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView champQuantite;
    ImageView buttonBidon;
    ImageView buttonBouteille;
    ImageView buttonVerre;
    ProgressBar progressBar;

    int qunatiteEauMaximale;
    int quantiteEauCourrante;
    int quantiteEauAjoute;

    Ecouteur ec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        qunatiteEauMaximale = 2000;
        quantiteEauCourrante = 0;

        champQuantite = findViewById(R.id.champQuantite);
        buttonBidon = findViewById(R.id.buttonBidon);
        buttonBouteille = findViewById(R.id.buttonBouteille);
        buttonVerre = findViewById(R.id.buttonVerre);
        progressBar = findViewById(R.id.progressBar);

        ec = new Ecouteur();

        buttonBidon.setOnClickListener(ec);
        buttonBouteille.setOnClickListener(ec);
        buttonVerre.setOnClickListener(ec);

    }

    private class Ecouteur implements View.OnClickListener{


        @Override
        public void onClick(View source) {
            if(source == buttonBidon){
                quantiteEauAjoute = 150;
                quantiteEauCourrante += quantiteEauAjoute;

            }else if(source == buttonBouteille){

                quantiteEauAjoute = 330;
                quantiteEauCourrante += quantiteEauAjoute;

            }else{ // buttonVerre

                quantiteEauAjoute = 1500;
                quantiteEauCourrante += quantiteEauAjoute;
            }

            champQuantite.setText(String.valueOf(quantiteEauCourrante) + " ml");

            if(quantiteEauCourrante <= qunatiteEauMaximale)
                progressBar.setProgress(quantiteEauCourrante);
            else
                progressBar.setProgress(qunatiteEauMaximale);
        }
    }
}