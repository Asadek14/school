package com.ahmed.sadek.annexe2;

public class Compte {
    private String nomCompte;
    private double solde;

    public Compte(String nomCompte, double solde){
        this.nomCompte = nomCompte;
        this.solde = solde;
    }

    public double getSolde() {
        return solde;
    }

    public String getNomCompte() {
        return nomCompte;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
}
