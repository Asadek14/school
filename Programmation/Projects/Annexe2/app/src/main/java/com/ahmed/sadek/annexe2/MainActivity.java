package com.ahmed.sadek.annexe2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    Spinner spinnerCompte;
    TextView texteSold;
    Button buttonEnvoyer;
    EditText champCourriel;
    EditText champTransfert;
    DecimalFormat df;
    Vector<String> typesCompte;
    Compte compteChoisi;


    double solde;

    HashMap<String, Compte> hm;

    //etape 1
    Ecouteur ec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialisation des components
        spinnerCompte = findViewById(R.id.spinnerCompte);
        texteSold = findViewById(R.id.textSolde);
        buttonEnvoyer = findViewById(R.id.buttonEnvoyer);
        champCourriel = findViewById(R.id.champcourriel);
        champTransfert = findViewById(R.id.champstranfert);


        hm = new HashMap<>();
        hm.put("CHEQUE", new Compte("CHEQUE", 500));
        hm.put("EPARGNE", new Compte("EPARGNE", 1000));
        hm.put("EPARGNE PLUS", new Compte("EPARGNE PLUS", 1500));

        typesCompte = new Vector();



        Set<String> listeComptes = hm.keySet();

        typesCompte.addAll(listeComptes);

        // initialisation du spinner

        ArrayAdapter adaptateur = new ArrayAdapter(this, android.R.layout.simple_list_item_1, typesCompte);
        spinnerCompte.setAdapter(adaptateur);

        // etape 1
        ec = new Ecouteur();

        // etape 2

        buttonEnvoyer.setOnClickListener(ec);
        spinnerCompte.setOnItemSelectedListener(ec);

        // initialser decimal format
        df = new DecimalFormat("#,000.00$");
    }

    // etape 3
    private class Ecouteur implements View.OnClickListener, AdapterView.OnItemSelectedListener {


        @Override
        public void onClick(View source) {

                String courriel = champCourriel.getText().toString();
                courriel = courriel.trim();
                if(courriel.length() != 0){

                    String montant = champTransfert.getText().toString();
                    if(montant.length() != 0 && courriel.matches("[.\\w]+@[.\\w]+")){

                        double montantTransfert = Double.parseDouble(montant);
                        if(montantTransfert <= compteChoisi.getSolde()) {
                            solde -= montantTransfert;
                            texteSold.setText(df.format(solde));
                            // changer le solde de compte
                            compteChoisi.setSolde(solde);

                        }
                    }
                    else{
                        texteSold.setText(String.valueOf("tu ne peut pas"));
                        champTransfert.setText("0");
                    }
                }
                else{

                    texteSold.setText("Veuillex entrer un courriel valide");
                    champTransfert.setText("0");
                }
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

            String hashCle;

            // le nom du compte selectionne
//          hashCle = adapterView.getSelectedItem().toString();
//          hashCle = spinnerCompte.getSelectedItem().toString();
//          hashCle = adapterView.getItemAtPosition(position).toString();
//          hashCle = ((TextView)view).getText().toString();
            hashCle = typesCompte.get(position);

            // l'objet correspondant au compte selectionnee
            compteChoisi = hm.get(hashCle);
            solde = compteChoisi.getSolde();


            // afficher le solde
            texteSold.setText(df.format(solde));

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}