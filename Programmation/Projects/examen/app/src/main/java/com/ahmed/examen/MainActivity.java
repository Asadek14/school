package com.ahmed.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    ImageButton buttonAvion, buttonHotel;
    TextView nbBillets, nbSemaines, coutTotal;
    Button buttonTotal;
    Commande commande;

    Ecouteur ec;

    DecimalFormat df;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAvion = findViewById(R.id.BUTTON_AVION);
        buttonHotel = findViewById(R.id.BUTTON_HOTEL);
        buttonTotal = findViewById(R.id.BUTTON_TOTAL);

        nbBillets = findViewById(R.id.NB_BILLETS);
        nbSemaines = findViewById(R.id.NB_SEMAINES);
        coutTotal = findViewById(R.id.COUT_TOTAL);

        commande = new Commande();

        ec = new Ecouteur();

        buttonAvion.setOnClickListener(ec);
        buttonHotel.setOnClickListener(ec);
        buttonTotal.setOnClickListener(ec);

        df = new DecimalFormat("#.00$");
    }

    private class Ecouteur implements View.OnClickListener{


        @Override
        public void onClick(View source) {
            if( source == buttonAvion){
                String value = nbBillets.getText().toString();
                Integer nbBilletCourrant = Integer.parseInt(value);
                commande.ajouterProduit(new HebergementHotel(1));
                nbBillets.setText((++nbBilletCourrant).toString());
            }
            else if(source == buttonHotel){
                String value = nbSemaines.getText().toString();
                Integer nbSemainesCourrant = Integer.parseInt(value);
                commande.ajouterProduit(new BilletAvion(1));
                nbSemaines.setText((++nbSemainesCourrant).toString());
            }else if(source == buttonTotal){
                coutTotal.setText(df.format(commande.grandTotal()));
            }
        }
    }
}