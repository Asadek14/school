package com.ahmed.examen;

import java.util.Vector;

public class Commande {

    private Vector<Produit> listeCommande;

    public Commande() {
        listeCommande = new Vector();
    }

    public void ajouterProduit(Produit p) {
        listeCommande.add(p);
    }

    public double total() {
        double total = 0;
        // compléter : total de la commande

        for(int i = 0; i < listeCommande.size(); i++){
            Produit p = listeCommande.get(i);
            total += p.getPrixUnitaire() * p.getQte();
        }

        return total;
    }

    public double taxes() {
        double taxes = 0;

        // compléter : montant des taxes sur le total de la commande

        double MontantTotal = this.total();
        // tps sur le montant avant taxes ( 5% )

        double tps = MontantTotal * 0.05;
        //tvq sur le montant avant taxes ( 9.975% )

        double tvq = MontantTotal * 0.09975;
        // taxes total = tps + tvq

        taxes = tps + tvq;
        return taxes;
    }

    public double grandTotal() {

        double grTotal = 0;

        // compléter

        grTotal = this.total() + this.taxes();

        return grTotal;
    }
}
