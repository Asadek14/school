package com.ahmed.annexe12;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.Vector;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance; // unique instance of the Singleton
    private SQLiteDatabase database;

    public static DatabaseHelper getInstance(Context context){
        if(instance == null)
            instance = new DatabaseHelper(context.getApplicationContext());
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, "db", null, 1);  // context, name, factory, version
//        ouvrirBD();

    }

    public void ajouterInventeur( Inventeur e, SQLiteDatabase sqLiteDatabase){

        ContentValues cv = new ContentValues();
        cv.put("nom", e.getNom());
        cv.put("origine", e.getOrigine());
        cv.put("invention", e.getInvention());
        cv.put("annee", e.getAnnee());

        sqLiteDatabase.insert("inventeur", null, cv);
    }


    // called automatically when the app is installed ( 1 time )
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE inventeur (_id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, origine TEXT, invention TEXT, annee INTEGER);"); // create the table
        ajouterInventeur(new Inventeur("Laszlo Biro", "Hongrie", "Stylo à bille", 1938), sqLiteDatabase);
        ajouterInventeur(new Inventeur("Benjamin Franklin", "Etats-Unis", "Paratonnerre", 1752), sqLiteDatabase);
        ajouterInventeur(new Inventeur("Mary Anderson", "Etats-Unis", "Essuie-glace", 1903), sqLiteDatabase);
        ajouterInventeur(new Inventeur("Grace Hopper", "Etats-Unis", "Compilateur", 1952), sqLiteDatabase);
        ajouterInventeur(new Inventeur("Benoit Rouquayrot", "France", "Scaphandre", 1864), sqLiteDatabase);
    }


    // called automatically when the phone detects that 1 database with the same name already exists on the phone
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int ancVersion, int nlleVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS inventeur");
        onCreate(sqLiteDatabase);

    }

    public void ouvrirBD(){
        database = this.getWritableDatabase();
    }

    public void fermerBD(){
        database.close(); // fermer la connexion
    }

    public Vector<String> retournerInventions (){

        Vector<String> v = new Vector<>();
        Cursor c = database.rawQuery("SELECT invention FROM inventeur", null);

        while(c.moveToNext()){ // tant qu'il y a des resultat encore
            v.add(c.getString(0)); // j'ajoute l'invention dans le vecteur
        }
        return v;
    }

    public Boolean vraiInventeur(String inventeur, String invention){

        String[] tab = { invention, inventeur };
        Cursor c = database.rawQuery("SELECT invention FROM inventeur WHERE invention = ? AND nom = ?", tab);
        return c.moveToNext(); // vrai ou faux
    }
}