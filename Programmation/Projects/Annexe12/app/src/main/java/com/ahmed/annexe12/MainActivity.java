package com.ahmed.annexe12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    TextView vraiOuFaux;
    ListView reponses;
    DatabaseHelper instance;
    Ecouteur ec;
    Vector<String> choix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vraiOuFaux = findViewById(R.id.VraiOuFaux);
        reponses = findViewById(R.id.Responses);





    }

    @Override
    protected void onStop(){
        super.onStop();
        instance.fermerBD();
    }

    @Override
    protected void onStart(){
        super.onStart();

        // remplir la liste avec le choix de reponses
        instance = DatabaseHelper.getInstance(this);
        instance.ouvrirBD();



        choix = instance.retournerInventions();

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, choix);

        reponses.setAdapter(adapter);

        ec = new Ecouteur();
        reponses.setOnItemClickListener(ec);
    }

    private class Ecouteur implements AdapterView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            String inventeur = "Mary Anderson";
            Boolean isTrue = instance.vraiInventeur(inventeur, choix.get(i));
            vraiOuFaux.setText(isTrue.toString());

        }
    }



}