DROP TABLE IF EXISTS suivage;
DROP TABLE IF EXISTS amour;
ALTER TABLE IF EXISTS utilisateur
	DROP CONSTRAINT fk_tweet_favori_tweet;
DROP TABLE IF EXISTS tweet;
DROP TABLE IF EXISTS utilisateur;

CREATE TABLE utilisateur(
	email VARCHAR(320) PRIMARY KEY,
	handle VARCHAR(15) UNIQUE NOT NULL,
	pseudo VARCHAR(20) NOT NULL,
	photo BYTEA,
	tweet_favori INT NULL
);

CREATE TABLE tweet(
	ID SERIAL,
	texte VARCHAR(140) NOT NULL,
	retweet INT,
	twit VARCHAR(320) NOT NULL,
	reply INT,
	CONSTRAINT pk_ID PRIMARY KEY (ID),
	CONSTRAINT fk_twit_utilisateur FOREIGN KEY(twit) REFERENCES utilisateur(email),
	CONSTRAINT fk_retweet_tweet FOREIGN KEY(retweet) REFERENCES tweet(ID),
	CONSTRAINT fk_reply_tweet FOREIGN KEY(reply) REFERENCES tweet(ID)	
);

ALTER TABLE utilisateur
ADD CONSTRAINT fk_tweet_favori_tweet FOREIGN KEY(tweet_favori) REFERENCES tweet(ID);

CREATE TABLE amour(
	ID SERIAL PRIMARY KEY,
	aimeur VARCHAR(320),
	texte_aime INT,
	CONSTRAINT fk_aimeur_email FOREIGN KEY(aimeur) REFERENCES utilisateur(email)
);

ALTER TABLE amour
ADD CONSTRAINT fk_tweet_aime_tweet FOREIGN KEY(texte_aime) REFERENCES tweet(ID);

CREATE TABLE suivage(
	ID INT,
	suivi VARCHAR(320),
	suiveux VARCHAR(320),
	CONSTRAINT fk_suivi_email FOREIGN KEY(suivi) REFERENCES utilisateur(email),
	CONSTRAINT fk_suiveur_email FOREIGN KEY(suiveux) REFERENCES utilisateur(email)
)


