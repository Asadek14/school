DROP TABLE IF EXISTS accompagnement_commande;
DROP TABLE IF EXISTS pizza_commande;
DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS planete;
DROP TABLE IF EXISTS pizza_format;
DROP TABLE IF EXISTS format;
DROP TABLE IF EXISTS pizza;
DROP TABLE IF EXISTS accompagnement;

CREATE TABLE pizza(
	sigle CHAR(4) PRIMARY KEY,
	nom VARCHAR(50)
);

CREATE TABLE format(
	lettre CHAR(1) PRIMARY KEY,
	nom VARCHAR(20)
);

CREATE TABLE pizza_format(
	id SERIAL PRIMARY KEY,
	sigle_pizza CHAR(4),
	lettre_format CHAR(1),
	prix decimal(4,2),
	CONSTRAINT fk_pizza_format_pizza FOREIGN KEY(sigle_pizza) REFERENCES pizza(sigle)
	CONSTRAINT fk_pizza_format_format FOREIGN KEY (lettre_format) REFERENCES format(lettre)
);

CREATE TABLE accompagnement(
	sigle CHAR(4) PRIMARY KEY,
	nom VARCHAR(50),
	prix DECIMAL(4,2)
);

CREATE TABLE planete(
	id SERIAL PRIMARY KEY,
	nom VARCHAR(100)
);

CREATE TABLE client(
	id SERIAL PRIMARY KEY,
	prenom VARCHAR(100),
	nom VARCHAR(100),
	planete INT,
	CONSTRAINT fk_client_planete FOREIGN KEY(planete) REFERENCES planete(id)
	
);

CREATE TABLE commande(
 id SERIAL PRIMARY KEY,
 client INT,
 date_emission TIMESTAMP,
 CONSTRAINT fk_commande_client FOREIGN KEY(client) REFERENCES client(id)
);

CREATE TABLE pizza_commande(
id SERIAL PRIMARY KEY,
 pizza_format INT,
 commande INT,
 CONSTRAINT fk_pizza_commande_pizza FOREIGN KEY(pizza_format) REFERENCES pizza_format(id),
 CONSTRAINT fk_pizza_commande_commande FOREIGN KEY(commande) REFERENCES commande(id)
);

CREATE TABLE accompagnement_commande(
id SERIAL PRIMARY KEY,
 accompagnement CHAR(4),
 commande INT,
 CONSTRAINT fk_accompagnement_commande_accompagnement FOREIGN KEY(accompagnement) REFERENCES accompagnement(sigle),
 CONSTRAINT fk_accompagnement_commande_commande FOREIGN KEY(commande) REFERENCES commande(id)
);


INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Chou', 'Salade de chou', 4.25);

INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Frit', 'Gluxons frits', 5.50);

INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Oign', 'Shphéroïdes d''oignons', 6.35);

INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Bois', 'Boisson plasma', 5);

INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Milk', 'Milkshake au plutonium', 32.30);

INSERT INTO public.accompagnement(
sigle, nom, prix)
VALUES ('Mout', 'Cataplasme de moutarde', 12.12);

INSERT INTO public.planete(nom)
	VALUES ('Klaxxon 5');

INSERT INTO public.planete(nom)
	VALUES ('Orbulix');

INSERT INTO public.planete(nom)
	VALUES ('Plan nordorr');

INSERT INTO public.planete(nom)
	VALUES ('Tolkien');

INSERT INTO public.planete(nom)
	VALUES ('Saint-Hyppolite');

INSERT INTO public.planete(nom)
	VALUES ('Planète x');

INSERT INTO public.planete(nom)
	VALUES ('Grande Ourse');


INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Phrénon', 'Balak', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Louis-Charles', 'Déziel', (select id from public.planete order by random() limit 1));
    
INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Xor', 'Balak', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Ringo', 'Starr', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('PFFFRXTH', 'Tremblay', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Glastron', 'Mliron', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Miranda', 'Lawson', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Rex', 'Ruthor', (select id from public.planete order by random() limit 1));

INSERT INTO public.client(prenom, nom, planete)
	VALUES ('Ronald', 'McDonald', (select id from public.planete order by random() limit 1));

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Pepe', 'Pepperoni Fermenté');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('AllD', 'All diseases');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Hawa', 'How are you? Good thank you');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Delu', 'Delucieuse');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Vian', 'Viande à chien Donalda');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Vege', 'Végéter au centre d''achat');

INSERT INTO public.pizza(
	sigle, nom)
	VALUES ('Marg', 'Margarine et poivre');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('S', 'Small');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('M', 'Medium');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('L', 'Large');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('X', 'XLarge');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('J', 'Jumbo');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('P', 'Paranormal');

INSERT INTO public.format(
	lettre, nom)
	VALUES ('G', 'Galactique');

INSERT INTO pizza_format(sigle_pizza, lettre_format, prix)
SELECT sigle, lettre, Random()*50::DECIMAL(4,2) FROM pizza
CROSS JOIN format;

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));
       
INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));

INSERT INTO public.commande(client, date_emission)
	VALUES ((SELECT id FROM client ORDER BY random() LIMIT 1),(select timestamp '2014-01-10 20:00:00' +
       random() * INTERVAL '100000 days'));
	   
	   
INSERT INTO public.accompagnement_commande(accompagnement, commande)
SELECT sigle, id
FROM accompagnement
cross join commande
ORDER BY random()
LIMIT 40;

INSERT INTO public.pizza_commande(pizza_format, commande)	
SELECT pizza_format.id, commande.id
FROM pizza_format
cross join commande
ORDER BY random()
LIMIT 100;

