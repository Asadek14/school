SELECT 2 * PI() as "2XPI",
			PI() as "PI", 
			PI() / 2 as "PI/2" 
FROM cercle;


SELECT RANDOM() as "[0,1]",
		RANDOM() * 100 AS "[0,100]",
		RANDOM() * 2 - 1 AS "[-1,1]",
		RANDOM() * 1500 - 1000 AS "[-1000,500]"
FROM cercle;

SELECT * FROM cercle;

SELECT id, cx || ',' || cy AS "position centre"
		,rayon
		,2*PI()*rayon AS "perimetre"
		,PI()*rayon^2 AS "aire"
FROM cercle;

SELECT id
		, x2-x1 || ',' || y2-y1 AS "position centre"
		,(y2-y1)/(x2-x1) AS "pente"
		,|/(y2-y1)^2 + (x2-x1)^2 AS "longeur"
FROM ligne;
		
SELECT id
		,dx || ',' || dy
		,

FROM vecteur;