SELECT * FROM employe;

SELECT nom, prenom, salaire FROM employe;

SELECT nom, prenom, NOW() FROM employe;

SELECT 1;

SELECT 'y fa frette !', 2 * 3;

SELECT prenom AS "Prénom", nom AS "Nom" FROM employe;

SELECT nom AS "Nom", nom AS "Nom" FROM employe;

SELECT * FROM employe WHERE nom = 'Dupuis';

SELECT * FROM employe WHERE salaire >= 20.0;

SELECT * FROM employe WHERE nom = 'Brochant' AND salaire > 20.0;

SELECT DISTINCT nom FROM employe WHERE nom = 'Brochant';

SELECT nom, prenom, ville
FROM employe
WHERE ville IN ('Laval', 'Longueuil');