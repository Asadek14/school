-- 1.1
SELECT * FROM employe;

-- 1.2
SELECT nom, prenom, departement FROM employe;

--1.3
SELECT departement, prenom, nom FROM employe;

--1.4
SELECT nom, prenom FROM employe WHERE departement = 'ventes';

--1.5
-- SELECT 
-- nom AS "Nom de l'employé", 
-- prenom AS "Prénom de l'employé", 
-- salaire AS "Salaire de l'employé" 
-- FROM employe
-- WHERE salaire > 20.000; 

--1.6
SELECT nom, prenom, departement, salaire 
FROM employe WHERE departement = 'ventes' 
AND salaire > 20.000;

--1.7
SELECT * FROM employe 
WHERE departement = 'r&d' 
OR (ville = 'Montréal'
AND salaire >= 25.00);

--1.8
SELECT nom, prenom, ville
FROM employe
WHERE ville IN ('Laval', 'Longueuil');

--1.9
SELECT nom, prenom, departement FROM employe WHERE superviseur = '111';

--1.10
SELECT nom, prenom, salaire
FROM employe 
WHERE salaire BETWEEN 20.00 AND 30.000;

SELECT nom, prenom, salaire
FROM employe 
WHERE salaire >= 20.00 AND salaire <= 30.000;

--1.11
SELECT nom, prenom, salaire
FROM employe 
WHERE salaire BETWEEN 20.01 AND 29.99;

SELECT nom, prenom, salaire
FROM employe 
WHERE salaire > 20.00 AND salaire < 30.000;

--1.12
SELECT nom, prenom
FROM employe
WHERE nom BETWEEN 'Lebel' AND 'Tangay';

--1.13
SELECT nom FROM employe;

SELECT DISTINCT nom FROM employe;

--1.14
SELECT 1;

--1.15
SELECT nom, prenom, 'employé' AS "cquilfait"
FROM employe;

--1.16
SELECT nom, prenom, salaire, 
		ROUND(salaire * 0.05,2) AS "montant",
		ROUND(salaire * 1.05,2) AS "montantAjoute"
FROM employe;

--1.17
SELECT nom, prenom, departement
FROM employe
WHERE departement NOT IN ('ventes', 'achats');

--1.18





